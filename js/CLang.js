TEXT_GAMEOVER  = "Game Berakhir";
TEXT_SCORE     = "SCORE";
TEXT_PLAY      = "Mulai";
TEXT_HELP1     = "Gunakan Arrow di Keyboard Untuk menggerakan Spaceship";
TEXT_HELP2     = "Tekan Spacebar untuk menembakan senjata!";
TEXT_HELP1_MOBILE     = "Gunakan Arrow di Keyboard Untuk menggerakan Spaceship";
TEXT_HELP2_MOBILE     = "Tekan Tombol Shoot untuk menembakan senjata!";
TEXT_SHOOT     = "Tembak";
var TEXT_DEVELOPED = "developed by";
var TEXT_PRELOADER_CONTINUE = "START";

TEXT_SHARE_IMAGE = "200x200.jpg";
TEXT_SHARE_TITLE = "Congratulations!";
TEXT_SHARE_MSG1 = "You collected <strong>";
TEXT_SHARE_MSG2 = " points</strong>!<br><br>Share your score with your friends!";
TEXT_SHARE_SHARE1 = "My score is ";
TEXT_SHARE_SHARE2 = " points! Can you do better?";