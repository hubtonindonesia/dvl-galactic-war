function CEndPanel(oSpriteBg){
    
    var _oBg;
    var _oScoreTextBack;
    var _oScoreText;
    var _oMsgText;
    var _oMsgTextBack;
    var _oAjaxTextBack;
    var _oAjaxText;
    var _oGroup;
    
    this._init = function(oSpriteBg){
        _oBg = createBitmap(oSpriteBg);

        _oMsgTextBack = new createjs.Text("","40px " + FONT_GAME, "#000");
        _oMsgTextBack.x = CANVAS_WIDTH/2 +1;
        _oMsgTextBack.y = (CANVAS_HEIGHT/2)-160;
        _oMsgTextBack.textAlign = "center";

        _oMsgText = new createjs.Text("","40px "+FONT_GAME, "#ffffff");
        _oMsgText.x = CANVAS_WIDTH/2;
        _oMsgText.y = (CANVAS_HEIGHT/2)-162;
        _oMsgText.textAlign = "center";
        
        _oScoreTextBack = new createjs.Text("","32px "+FONT_GAME, "#000");
        _oScoreTextBack.x = CANVAS_WIDTH/2 +1;
        _oScoreTextBack.y = (CANVAS_HEIGHT/2) - 58;
        _oScoreTextBack.textAlign = "center";
        
        _oScoreText = new createjs.Text("","32px "+FONT_GAME, "#ffffff");
        _oScoreText.x = CANVAS_WIDTH/2;
        _oScoreText.y = (CANVAS_HEIGHT/2) - 60;
        _oScoreText.textAlign = "center";

        _oAjaxTextBack = new createjs.Text("", "20px "+FONT_GAME, "#000");
        _oAjaxTextBack.x = CANVAS_WIDTH/2;
        _oAjaxTextBack.y = (CANVAS_HEIGHT/2) + 12;
        _oAjaxTextBack.textAlign = "center";
        _oAjaxTextBack.lineWidth = 550

        _oAjaxText = new createjs.Text("", "20px "+FONT_GAME, "#ffffff");
        _oAjaxText.x = CANVAS_WIDTH/2;
        _oAjaxText.y = (CANVAS_HEIGHT/2) + 10;
        _oAjaxText.textAlign = "center";
        _oAjaxText.lineWidth = 550
        
        _oGroup = new createjs.Container();
        _oGroup.alpha = 0;
        _oGroup.visible=false;
        
        _oGroup.addChild(_oBg, _oScoreTextBack, _oScoreText, _oMsgTextBack, _oMsgText, _oAjaxText);

        s_oStage.addChild(_oGroup);
    };
    
    this.unload = function(){
        _oGroup.off("mousedown",this._onExit);
    };
    
    this._initListener = function(){
        // _oGroup.on("mousedown",this._onExit);
        // $(s_oMain).trigger("show_interlevel_ad");
    };
    
    this.show = function(iScore){
        _oMsgTextBack.text = TEXT_GAMEOVER;
        _oMsgText.text = TEXT_GAMEOVER;
        
        _oScoreTextBack.text = TEXT_SCORE +": "+iScore;
        _oScoreText.text = TEXT_SCORE +": "+iScore;

        console.log(iScore)

        // Send Score
        // var scoreJSON = JSON.stringify(iScore);
        // var urlAjax =  "http://localhost/sendScore=" + iScore;
        // request = $.ajax({
        //     url: urlAjax,
        //     type: "POST",
        //     data: scoreJSON
        // });
        /* MOD */
        this.sendScore(iScore);

        _oGroup.visible = true;

        var oParent = this;
        createjs.Tween.get(_oGroup).to({alpha:1 }, 500).call(function() {oParent._initListener();});

        $(s_oMain).trigger("save_score",iScore);
    };

    /* @MOD */
    this.searchToObject = function(){
      var pairs = window.location.search.substring(1).split("&"),
        obj = {},
        pair,
        i;

      for ( i in pairs ) {
        if ( pairs[i] === "" ) continue;

        pair = pairs[i].split("=");
        obj[ decodeURIComponent( pair[0] ) ] = decodeURIComponent( pair[1] );
      }

      return obj;
    }

    /* @MOD */
    this.sendScore = function (score){
      /* @MOD send score to backend */
      var searchQuery = this.searchToObject();
      var gameId = searchQuery.gameId;
      var token = searchQuery.token;

      console.log(token, gameId)

      /* BASE_ENDPOINT located at settings.js */
      $.ajax({
        type: "POST",
        url: BASE_ENDPOINT+'/games/'+gameId+'/submit-score',
        data: {score:score},
        headers: {
          Authorization: "Bearer "+token
        },
        success: function (result) {
          if(result.message){
            // alert(result.message);
            _oAjaxText.text = result.message + '\nPlease close this modal to exit';
            _oAjaxTextBack.text = result.message + '\nPlease close this modal to exit';
          } else {
            // alert("Score successfully saved.");
            _oAjaxText.text = 'Score successfully saved.\nPlease close this modal to exit';
            _oAjaxTextBack.text = 'Score successfully saved.\nPlease close this modal to exit';
          }

          console.log(result);
        },
        error: function (err) {
          if(err.responseJSON && err.responseJSON.message){
            // alert(err.responseJSON.message);
            _oAjaxText.text = 'Error: ' + err.responseJSON.message + '\nPlease close this modal to try again';
            _oAjaxTextBack.text = 'Error: ' + err.responseJSON.message + '\nPlease close this modal to try again';
          } else {
            console.error(err);
            _oAjaxText.text = 'Error\nPlease close this modal to try again';
            _oAjaxTextBack.text = 'Error\nPlease close this modal to try again';
          }
        }
      });
    };
    
    this._onExit = function(){
        _oGroup.off("mousedown",this._onExit);
        s_oStage.removeChild(_oGroup);
        
        s_oGame.onExit();
    };
    
    this._init(oSpriteBg);
    
    return this;
}